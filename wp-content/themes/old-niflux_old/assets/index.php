<?php
get_header();
?>
<main class="main">
            <div class="intro-slider-container">
               <div class="owl-carousel owl-simple owl-light owl-nav-inside owl-loaded owl-drag" data-toggle="owl" data-owl-options="{&quot;nav&quot;: false}">
                  <!-- End .intro-slide -->
                  <!-- End .intro-slide -->
                  <!-- End .intro-slide -->
                  <div class="owl-stage-outer">
                     <div class="owl-stage" style="transform: translate3d(-2698px, 0px, 0px); transition: all 0s ease 0s; width: 9443px;">
                        <div class="owl-item" style="width: 1349px;">
                           <div class="intro-slide" style="background-image: url(img/banner.png);">
                              <div class="container intro-content">
                                 <h3 class="intro-subtitle"><b>NiFLUX</b> The Range of</h3>
                                 <!-- End .h3 intro-subtitle -->
                                 <h1 class="intro-title">FILAMENT BULBS</h1>
                                 <!-- End .intro-title -->
                                 <a href="#" class="btn btn-primary">
                                 <span>Shop Now</span>
                                 </a>
                              </div>
                              <!-- End .container intro-content -->
                           </div>
                        </div>
						<div class="owl-item" style="width: 1349px;">
                           <div class="intro-slide" style="background-image: url(img/banner2.png);">
                              <div class="container intro-content">
                                 <h3 class="intro-subtitle"><b>NiFLUX</b> The Range of</h3>
                                 <!-- End .h3 intro-subtitle -->
                                 <h1 class="intro-title">FILAMENT BULBS</h1>
                                 <!-- End .intro-title -->
                                 <a href="#" class="btn btn-primary">
                                 <span>Shop Now</span>
                                 </a>
                              </div>
                              <!-- End .container intro-content -->
                           </div>
                        </div>
                     </div>
                  </div>
                  
               </div>
               <!-- End .owl-carousel owl-simple -->
               <span class="slider-loader text-white"></span><!-- End .slider-loader -->
            </div>
            <!-- End .intro-slider-container -->
            
            <!-- End .owl-carousel -->
            <div class="mb-3 mb-lg-5"></div>
            <!-- End .mb-3 mb-lg-5 -->
            <div class="banner-group">
               <div class="container">
                  <div class="row">
                     <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="banner banner-large banner-overlay banner-overlay-light">
                           <a href="#">
                           <img src="img/image3.jpg" alt="Banner">
                           </a>
                           <div class="banner-content banner-content-top ">
                              <!-- End .banner-subtitle -->
                              <h3 class="banner-title">FILAMENT BULBS</h3>
                              <!-- End .banner-title -->
                              <!-- End .banner-text -->
                              <a href="#" class="btn banner-link">Shop Now<img src="img/left-arrow.png"/></a>
                           </div>
                           <!-- End .banner-content -->
                        </div>
                        <!-- End .banner -->
                     </div>
                     <!-- End .col-lg-5 -->
                     <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="banner banner-overlay">
                           <a href="#">
                           <img src="img/image2.jpg" alt="Banner">
                           </a>
                           <div class="banner-content banner-content-top bannerRightContent">
                              <!-- End .banner-subtitle -->
                              <h3 class="banner-title text-white">LED </h3>
                              <!-- End .banner-title -->
                              <div class="banner-text ">Surface Panel</div>
                              <!-- End .banner-text -->
                              <a href="#" class="btn banner-link">Shop Now<img src="img/left-arrow.png"/></a>
                           </div>
                           <!-- End .banner-content -->
                        </div>
                        <!-- End .banner -->
                     </div>
                     <!-- End .col-lg-3 -->
                     <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="banner banner-overlay">
                           <a href="#">
                           <img src="img/image1.jpg" alt="Banner">
                           </a>
                           <div class="banner-content banner-content-top rightLastContent">
                              <!-- End .banner-subtitle -->
                              <h3 class="banner-title text-white">FILAMENT <br> BULBS</h3>
                              <!-- End .banner-title -->
                              <a href="#" class="btn banner-link">Shop Now<img src="img/white-arrow.png"/></a>
                           </div>
                           <!-- End .banner-content -->
                        </div>
                        <!-- End .banner -->
                     </div>
                     <!-- End .col-lg-4 -->
                  </div>
                  <!-- End .row -->
               </div>
               <!-- End .container -->
            </div>
            <!-- End .banner-group -->
            <div class="mb-3"></div>
            <!-- End .mb-6 -->
            <div class="container">
               <h4 class="sectionTitle">Bulb & Fliament Bulb</h4>
            </div>
            <!-- End .container -->
            <div class="container">
               <div class="owl-carousel owl-theme customizeArrows">
				<div class="item">
					<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-1.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
					<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-6.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Filament Bulb <b>"HORN"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="item">
						<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-2.jpg"/></a>
								<span class="newProduct floatingTag">New</span>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Bulb <b>"POWER"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
					<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-7.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Filament Bulb <b>"COPPER HORN"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
				
				</div>
				<div class="item">
						<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-3.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Filament Bulb
 				<b>"TWINKLE"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
					<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-8.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Filament Bulb <b>"COPPER BALL"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
				
				</div>
				<div class="item">
						<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-4.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Filament Bulb <b>"FLAME"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
					<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-9.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Filament Bulb <b>"COPPER PALLA"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
				
				</div>
				<div class="item">
						<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-5.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Filament Bulb<b>"DROPLET"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
					<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-1.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
				
				</div>
				<div class="item">
						<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-1.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
					<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-1.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
				
				</div>
				<div class="item">
						<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-1.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
					<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-1.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
				
				</div>
				<div class="item">
						<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-1.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
					<div class="product-section">
						<div class="product-content">
							
							<div class="productImage">
								<a href="#"><img src="img/product-1-1.jpg"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate">$40.00</span>									
									<span class="btn-buy float-right"><a href="#">buy now</a></span>
								</div>
							</div>
							
						</div>
					</div>
				
				</div>
				
			</div>
            </div>
            <!-- End .container-fluid -->
            <div class="mb-5"></div>
            <!-- End .mb-5 -->
            <div class="deal-container  mb-5">
               
					<div class="categoryShop pt-5 pb-15">
						<div class="container">
						   <div class="heading heading-center mb-3">
							  <h2 class="title">Shop By Category</h2>
							  <!-- End .title -->
						   </div>
						   <!-- End .heading -->
							<div class="categorySlider">
								   <div class="owl-carousel owl-theme customizeArrows">
									<div class="item">
										<div class="product-section">
											<div class="product-content">
												
												<div class="catImage">
													<a href="#"><img src="img/cat1.jpg"/></a>
												</div>
												<div class="cat-data">
													<div class="productName">
														<h4><a href="#">LED Flood Light <br><b>"FOCUS"</b></a></h4>
													</div>
												</div>
												
											</div>
										</div>
									</div>
									<div class="item">
										<div class="product-section">
											<div class="product-content">
												
												<div class="catImage">
													<a href="#"><img src="img/cat2.jpg"/></a>
												</div>
												<div class="cat-data">
													<div class="productName">
														<h4><a href="#">LED COB Downlight <br><b>"CLASSIC"</b></a></h4>
													</div>
												</div>
												
											</div>
										</div>
									
									</div>
									<div class="item">
										<div class="product-section">
											<div class="product-content">
												
												<div class="catImage">
													<a href="#"><img src="img/cat3.jpg"/></a>
												</div>
												<div class="cat-data">
													<div class="productName">
														<h4><a href="#">LED Street Lights <br><b>"STANDARD"</b></a></h4>
													</div>
												</div>
												
											</div>
										</div>
									
									</div>
									<div class="item">
										<div class="product-section">
											<div class="product-content">
												
												<div class="catImage">
													<a href="#"><img src="img/cat4.jpg"/></a>
												</div>
												<div class="cat-data">
													<div class="productName">
														<h4><a href="#">LED T8 Tubes  <br><b>"GLASSIC"</b></a></h4>
													</div>
												</div>
												
											</div>
										</div>
									
									</div>
									<div class="item">
										<div class="product-section">
											<div class="product-content">
												
												<div class="catImage">
													<a href="#"><img src="img/cat5.jpg"/></a>
												</div>
												<div class="cat-data">
													<div class="productName">
														<h4><a href="#">LED Slim Panel <br><b>"FRISBEE"</b></a></h4>
													</div>
												</div>
												
											</div>
										</div>
									
									</div>
									<div class="item">
										<div class="product-section">
											<div class="product-content">
												
												<div class="catImage">
													<a href="#"><img src="img/cat3.jpg"/></a>
												</div>
												<div class="cat-data">
													<div class="productName">
														<h4><a href="#">LED Street Lights <br><b>"STANDARD"</b></a></h4>
													</div>
												</div>
												
											</div>
										</div>
									
									</div>
									
								</div>
								
						
							</div>
						   <!-- End .tab-content -->
						</div>
					</div>
                  <!-- End .row -->
				<div class="container">  
				  <div class="icon-boxes-container">
					   <div class="container">
						  <div class="row">
							 <div class="col-sm-4 col-lg-4">
								<div class="icon-box icon-box-side">
								   <span class="icon-box-icon text-white">
								   <i class="icon-rocket"></i>
								   </span>
								   <div class="icon-box-content">
									  <h3 class="icon-box-title">Free Fast Shipment</h3>
									  <!-- End .icon-box-title -->
									  <p>If You are unable use lorem ipsum</p>
								   </div>
								   <!-- End .icon-box-content -->
								</div>
								<!-- End .icon-box -->
							 </div>
							 <!-- End .col-sm-6 col-lg-3 -->
							 <div class="col-sm-4 col-lg-4">
								<div class="icon-box icon-box-side">
								   <span class="icon-box-icon text-white">
								   <i class="icon-thumbs-up"></i>
								   </span>
								   <div class="icon-box-content">
									  <h3 class="icon-box-title">Save 20% when you</h3>
									  <!-- End .icon-box-title -->
									  <p>There are many variationss</p>
								   </div>
								   <!-- End .icon-box-content -->
								</div>
								<!-- End .icon-box -->
							 </div>
							 <!-- End .col-sm-6 col-lg-3 -->
							 <div class="col-sm-4 col-lg-4">
								<div class="icon-box icon-box-side">
								   <span class="icon-box-icon text-white">
								   <i class="icon-life-ring"></i>
								   </span>
								   <div class="icon-box-content">
									  <h3 class="icon-box-title">24/7 Online Support</h3>
									  <!-- End .icon-box-title -->
									  <p>24/7 amazing services</p>
								   </div>
								   <!-- End .icon-box-content -->
								</div>
								<!-- End .icon-box -->
							 </div>
							 <!-- End .col-sm-6 col-lg-3 -->
						  </div>
						  <!-- End .row -->
					   </div>
					   <!-- End .container -->
					</div>
            <!-- End .icon-boxes-container -->
            
				  
				  
               </div>
               <!-- End .container -->
            </div>
            <!-- End .bg-light -->
            <div class="mb-6"></div>
            <!-- End .mb-6 -->
            <div class="container">
               <div class="heading heading-center mb-3">
                  <h2 class="sectionTitle">New Arrivals</h2>
                  <!-- End .title -->
               </div>
               <!-- End .heading -->
				<div class="arrivalsSlider">
					   <div class="owl-carousel owl-theme customizeArrows">
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						
					</div>
					
            
				</div>
               <!-- End .tab-content -->
            </div>
            <!-- End .container -->
			<div class="container">
               <div class="heading heading-center mb-3">
                  <h2 class="sectionTitle">Hot Deals</h2>
                  <!-- End .title -->
               </div>
               <!-- End .heading -->
				<div class="arrivalsSlider">
					   <div class="owl-carousel owl-theme customizeArrows">
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									
									<div class="productImage">
										<a href="#"><img src="img/product-1-1.jpg"/></a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="#">LED Bulb <b>"SOLDIER"</b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate">$40.00</span>									
											<span class="btn-buy float-right"><a href="#">buy now</a></span>
										</div>
									</div>
									
								</div>
							</div>
						
						</div>
						
					</div>
					
            
				</div>
               <!-- End .tab-content -->
            </div>
            <!-- End .blog-posts -->
         </main>
         <!-- End .main -->
<?php
get_footer();
?>