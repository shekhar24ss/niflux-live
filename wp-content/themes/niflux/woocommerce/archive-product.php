<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();
global $wpdb;
 ?>



<?php

//$queried_object = get_queried_object();
$term_id = get_queried_object()->term_id;
$args = array(
	'post_type'             => 'product',
	'post_status'           => 'publish',
	'ignore_sticky_posts'   => 1,
	'posts_per_page'        => '-1',
	'tax_query'             => array(
	array(
		'taxonomy'     		=> 'product_cat',
		'field' 			=> 'term_id', //This is optional, as it defaults to 'term_id'
		'terms'         	=> $term_id,
		'operator'      	=> 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
	),
	array(
		'taxonomy'      	=> 'product_visibility',
		'field'         	=> 'slug',
		'terms'         	=> 'exclude-from-catalog', // Possibly 'exclude-from-search' too
		'operator'      	=> 'NOT IN'
	)
)
);
$products = get_posts($args);
$homeCatCount = 0;
?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			
<article id="post-21" class="post-21 page type-page status-publish hentry">
	<header class="entry-header">
		<h1 class="entry-title">Listing Page</h1>			</header><!-- .entry-header -->
	<!-- .entry-content -->
</article><!-- #post-21 -->


<section class="MainContent mt-5 pt-5">
<div class="container">
<div class="row">

<div class="col-xl-3">

<div class="FiterPart">

<h4>Filter Code here</h4>
</div>
</div>

<div class="col-xl-9">


<div class="row">
			<?php 
				foreach($products as $product)
				{ //echo'<pre>'; print_r($product); die('ss');
					$regularPrice = get_post_meta( $product->ID, '_regular_price', true );
					$salePrice = get_post_meta( $product->ID, '_sale_price', true );	
					if($homeCatCount == 0 || $homeCatCount%2==0)
					{
						echo'<div class="item col-xl-3">';
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'full' );
					} ?>
					<div class="product-section">
						<div class="product-content">
							<div class="productImage">
								<a href="#"><img src="<?php echo $image[0];?>"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li><?php //echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="<?php the_permalink($product->ID); ?>"><?php echo $product->post_title; ?></b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate"><?php echo get_woocommerce_currency_symbol().' '.$regularPrice; ?></span>									
									<span class="btn-buy float-right"><a href="<?php the_permalink($product->ID); ?>">buy now</a></span>
								</div>
							</div>
						</div>
					</div>
					<?php 
					if($homeCatCount%2==1)
					{
						echo'</div>';
					}
					$homeCatCount++; 
				} 
			?>
		</div>
</div>
</div>

		
	</div>
	
			</section>





		</main><!-- #main -->
	</div><!-- #primary -->
</div>



	<?php get_footer(); ?>
