 <footer class="footer footer-2">
            <div class="container" >
               <div class="footer-newsletter">
                     <h3 class="title"><img src="<?php echo get_template_directory_uri().'/assets/';?>img/speaker.png"/> Subscribe for join us !</h3>
                     
					  <div class="subscribeField">
							<!--form action="#">
							   <div class="input-group">
								  <input type="email" class="form-control" placeholder="Enter your Email Address" aria-label="Email Adress" aria-describedby="newsletter-btn" required="">
								  <div class="input-group-append">
									 <button class="btn btn-primary" type="submit" id="newsletter-btn"><span>Subscribe</span></button>
								  </div>
							   </div>
							</form-->
                     <?php echo do_shortcode('[contact-form-7 id="96" title="Subscribe Homepage"]'); ?>
						 <!-- End .col-sm-10 offset-sm-1 col-lg-6 offset-lg-3 -->
					  </div>
					  <!-- End .row -->
               </div>
               <!-- End .container -->
            </div>
            <!-- End .footer-newsletter bg-image -->
            <div class="footer-middle">
               <div class="container">
                  <div class="row">
                     <div class="col-sm-12 col-lg-4">
                           <div class="widget widget-about">
                              <!--h4 class="widget-title">Contact Information</h4>
                              <div class="widget-about-info">
                                 <span class="widget-about-title">Call us 24/7 Free</span>
                                 <a href="tel:123456789">+0123 456 789</a>
                                 <p>001 Demo Street, India</p>
                              </div-->
                              <?php
                              $footer_contact = get_post(112); 
                              echo $trim_me = $footer_contact->post_content;
                              ?>
                              <!-- End .widget-about-info -->
                           </div>
                        <!-- End .widget about-widget -->
                     </div>
                     <!-- End .col-sm-12 col-lg-3 -->
                     <div class="col-sm-4 col-lg-3">
                        <div class="widget">
                           <h4 class="widget-title">Get To Know us</h4>
                           <!-- End .widget-title -->
                           <!--ul class="widget-list">
                              <li><a href="#">About Us</a></li>
                              <li><a href="#">Careers</a></li>
                              <li><a href="#">Press Releases</a></li>
                              <li><a href="#">Category</a></li>
                              <li><a href="#">Contact us</a></li>
                           </ul-->
                           <?php 
                              $args = array(
                                 'menu_class' => 'widget-list',        
                                 'menu' => '(footer-get-to-know)'
                              );
                              wp_nav_menu( $args ); 
                           ?>
                           <!-- End .widget-list -->
                        </div>
                        <!-- End .widget -->
                     </div>
                     <!-- End .col-sm-4 col-lg-3 -->
                     <div class="col-sm-4 col-lg-3">
                        <div class="widget">
                           <h4 class="widget-title">Connect with us</h4>
                           <!-- End .widget-title -->
                           <!--ul class="widget-list">
                              <li><a href="#">Facebook</a></li>
                              <li><a href="#">Twitter</a></li>
                              <li><a href="#">Instagram</a></li>
                              <li><a href="#">Youtube</a></li>
                              <li><a href="#">Linkedin</a></li>
						         </ul-->
                           <?php 
                              $args = array(
                                 'menu_class' => 'widget-list',        
                                 'menu' => '(footer-connect-with-us)'
                              );
                              wp_nav_menu( $args ); 
                           ?>
                           <!-- End .widget-list -->
                        </div>
                        <!-- End .widget -->
                     </div>
                     <!-- End .col-sm-4 col-lg-3 -->
                     <div class="col-sm-4 col-lg-2">
                        <div class="widget">
                           <h4 class="widget-title">Let us help you</h4>
                           <!-- End .widget-title -->
                           <!--ul class="widget-list">
                              <li><a href="#">Your Account</a></li>
                              <li><a href="#">Return Centre</a></li>
                              <li><a href="#">100% Purchase Protection</a></li>
                              <li><a href="#">App Download</a></li>
                              <li><a href="#">Help</a></li>
                           </ul-->
                           <?php 
                              $args = array(
                                 'menu_class' => 'widget-list',        
                                 'menu' => '(footer-let-us-help)'
                              );
                              wp_nav_menu( $args ); 
                           ?>
                           <!-- End .widget-list -->
                        </div>
                        <!-- End .widget -->
                     </div>
                     <!-- End .col-sm-64 col-lg-3 -->
                  </div>
                  <!-- End .row -->
               </div>
               <!-- End .container -->
            </div>
            <!-- End .footer-middle -->
            <div class="footer-bottom">
               <div class="container">
                  <p class="footer-copyright">Copyright © 2020 NIFLUX</p>
                  <!-- End .footer-copyright -->
                  <!-- End .footer-menu -->
                  <div class="social-icons social-icons-color">
                     <a href="#"><img src="<?php echo get_template_directory_uri().'/assets/';?>img/payments.png" alt="Payment methods" width="272" height="20"></a>
                  </div>
                  <!-- End .soial-icons -->
               </div>
               <!-- End .container -->
            </div>
            <!-- End .footer-bottom -->
         </footer>
         <!-- End .footer -->
      </div>
      <!-- End .page-wrapper -->
      <button id="scroll-top" title="Back to Top" class=""><i class="icon-arrow-up"></i></button>
      <!-- Mobile Menu -->
      <div class="mobile-menu-overlay"></div>
      <!-- End .mobil-menu-overlay -->
      <div class="mobile-menu-container mobile-menu-light">
         <div class="mobile-menu-wrapper">
            <span class="mobile-menu-close"><i class="icon-close"></i></span>
            <form action="#" method="get" class="mobile-search">
               <label for="mobile-search" class="sr-only">Search</label>
               <input type="search" class="form-control" name="mobile-search" id="mobile-search" placeholder="Search product ..." required="">
               <button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
            </form>
            <ul class="nav nav-pills-mobile nav-border-anim" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" id="mobile-menu-link" data-toggle="tab" href="#mobile-menu-tab" role="tab" aria-controls="mobile-menu-tab" aria-selected="true">Menu</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" id="mobile-cats-link" data-toggle="tab" href="#mobile-cats-tab" role="tab" aria-controls="mobile-cats-tab" aria-selected="false">Categories</a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane fade show active" id="mobile-menu-tab" role="tabpanel" aria-labelledby="mobile-menu-link">
                  <nav class="mobile-nav">
                     <ul class="mobile-menu">
                        <li class="active">
                           <a href="index.html">Home</a></li>
                        <li>
                           <a href="#">Shop</a>
                        </li>
                        <li>
                           <a href="#" class="sf-with-ul">Product<span class="mmenu-btn"></span></a>
                           <ul>
                              <li><a href="#">Filament Bulb</a></li>
                                        <li><a href="#">LED Bulb</a></li>
                                        <li><a href="#">LED Flood Light</a></li>
                                        <li><a href="#">LED COB Downlight</a></li>
                                        <li><a href="#">LED Street Lights</a></li>
                           </ul>
                        </li>
                        <li>
                           <a href="#">Services</a>
                        </li>
                        <li>
                           <a href="#">My Account<span class="mmenu-btn"></span></a>
                           <ul>
                              <li><a href="#">Login</a></li>
                              <li><a href="#">Signup</a></li>
                           </ul>
                        </li>
                     </ul>
                  </nav>
                  <!-- End .mobile-nav -->
               </div>
               <!-- .End .tab-pane -->
               <div class="tab-pane fade" id="mobile-cats-tab" role="tabpanel" aria-labelledby="mobile-cats-link">
                  <nav class="mobile-cats-nav">
                     <ul class="mobile-cats-menu">
                        <li><a href="#">Filament Bulbs</a></li>
                        <li><a href="#">LED Surface Panel</a></li>
                        <li><a href="#">LED Slim Panel Frisbees</a></li>
                        <li><a href="#">LED COB Downlight Classic</a></li>
                        <li><a href="#">LED Flood Light Focus</a></li>
                        <li><a href="#">LED Street Lights Standard </a></li>
                        <li><a href="#">LED Streer Lights Marvel</a></li>
                        <li><a href="#">LED T8 Tubes  Classic</a></li>
                     </ul>
                     <!-- End .mobile-cats-menu -->
                  </nav>
                  <!-- End .mobile-cats-nav -->
               </div>
               <!-- .End .tab-pane -->
            </div>
            <!-- End .tab-content -->
            <div class="social-icons">
               <a href="#" class="social-icon" target="_blank" title="Facebook"><i class="icon-facebook-f"></i></a>
               <a href="#" class="social-icon" target="_blank" title="Twitter"><i class="icon-twitter"></i></a>
               <a href="#" class="social-icon" target="_blank" title="Instagram"><i class="icon-instagram"></i></a>
               <a href="#" class="social-icon" target="_blank" title="Youtube"><i class="icon-youtube"></i></a>
            </div>
            <!-- End .social-icons -->
         </div>
         <!-- End .mobile-menu-wrapper -->
      </div>
      <!-- End .mobile-menu-container -->
      <!-- Sign in / Register Modal -->
      <div class="modal fade" id="signin-modal" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i class="icon-close"></i></span>
                  </button>
                  <div class="form-box">
                     <div class="form-tab">
                        <ul class="nav nav-pills nav-fill" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link active" id="signin-tab" data-toggle="tab" href="#signin" role="tab" aria-controls="signin" aria-selected="true">Sign In</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="false">Register</a>
                           </li>
                        </ul>
                        <div class="tab-content" id="tab-content-5">
                           <div class="tab-pane fade show active" id="signin" role="tabpanel" aria-labelledby="signin-tab">
                              <form action="#">
                                 <div class="form-group">
                                    <label for="singin-email">Username or email address *</label>
                                    <input type="text" class="form-control" id="singin-email" name="singin-email" required="">
                                 </div>
                                 <!-- End .form-group -->
                                 <div class="form-group">
                                    <label for="singin-password">Password *</label>
                                    <input type="password" class="form-control" id="singin-password" name="singin-password" required="">
                                 </div>
                                 <!-- End .form-group -->
                                 <div class="form-footer">
                                    <button type="submit" class="btn btn-outline-primary-2">
                                    <span>LOG IN</span>
                                    <i class="icon-long-arrow-right"></i>
                                    </button>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="signin-remember">
                                       <label class="custom-control-label" for="signin-remember">Remember Me</label>
                                    </div>
                                    <!-- End .custom-checkbox -->
                                    <a href="#" class="forgot-link">Forgot Your Password?</a>
                                 </div>
                                 <!-- End .form-footer -->
                              </form>
                              <div class="form-choice">
                                 <p class="text-center">or sign in with</p>
                                 <div class="row">
                                    <div class="col-sm-6">
                                       <a href="#" class="btn btn-login btn-g">
                                       <i class="icon-google"></i>
                                       Login With Google
                                       </a>
                                    </div>
                                    <!-- End .col-6 -->
                                    <div class="col-sm-6">
                                       <a href="#" class="btn btn-login btn-f">
                                       <i class="icon-facebook-f"></i>
                                       Login With Facebook
                                       </a>
                                    </div>
                                    <!-- End .col-6 -->
                                 </div>
                                 <!-- End .row -->
                              </div>
                              <!-- End .form-choice -->
                           </div>
                           <!-- .End .tab-pane -->
                           <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                              <form action="#">
                                 <div class="form-group">
                                    <label for="register-email">Your email address *</label>
                                    <input type="email" class="form-control" id="register-email" name="register-email" required="">
                                 </div>
                                 <!-- End .form-group -->
                                 <div class="form-group">
                                    <label for="register-password">Password *</label>
                                    <input type="password" class="form-control" id="register-password" name="register-password" required="">
                                 </div>
                                 <!-- End .form-group -->
                                 <div class="form-footer">
                                    <button type="submit" class="btn btn-outline-primary-2">
                                    <span>SIGN UP</span>
                                    <i class="icon-long-arrow-right"></i>
                                    </button>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="register-policy" required="">
                                       <label class="custom-control-label" for="register-policy">I agree to the <a href="#">privacy policy</a> *</label>
                                    </div>
                                    <!-- End .custom-checkbox -->
                                 </div>
                                 <!-- End .form-footer -->
                              </form>
                              <div class="form-choice">
                                 <p class="text-center">or sign in with</p>
                                 <div class="row">
                                    <div class="col-sm-6">
                                       <a href="#" class="btn btn-login btn-g">
                                       <i class="icon-google"></i>
                                       Login With Google
                                       </a>
                                    </div>
                                    <!-- End .col-6 -->
                                    <div class="col-sm-6">
                                       <a href="#" class="btn btn-login  btn-f">
                                       <i class="icon-facebook-f"></i>
                                       Login With Facebook
                                       </a>
                                    </div>
                                    <!-- End .col-6 -->
                                 </div>
                                 <!-- End .row -->
                              </div>
                              <!-- End .form-choice -->
                           </div>
                           <!-- .End .tab-pane -->
                        </div>
                        <!-- End .tab-content -->
                     </div>
                     <!-- End .form-tab -->
                  </div>
                  <!-- End .form-box -->
               </div>
               <!-- End .modal-body -->
            </div>
            <!-- End .modal-content -->
         </div>
         <!-- End .modal-dialog -->
      </div>
     <!-- Plugins JS File -->
      <script src="<?php echo get_template_directory_uri().'/assets/';?>js/jquery.min.js"></script>
      <script src="<?php echo get_template_directory_uri().'/assets/';?>js/bootstrap.bundle.min.js"></script>
      <script src="<?php echo get_template_directory_uri().'/assets/';?>js/jquery.hoverIntent.min.js"></script>
      <script src="<?php echo get_template_directory_uri().'/assets/';?>js/jquery.waypoints.min.js"></script>
      <script src="<?php echo get_template_directory_uri().'/assets/';?>js/superfish.min.js"></script>
      <script src="<?php echo get_template_directory_uri().'/assets/';?>js/owl.carousel.min.js"></script>
      <script src="<?php echo get_template_directory_uri().'/assets/';?>js/jquery.plugin.min.js"></script>
      <script src="<?php echo get_template_directory_uri().'/assets/';?>js/jquery.magnific-popup.min.js"></script>
      <script src="<?php echo get_template_directory_uri().'/assets/';?>js/jquery.countdown.min.js"></script>
      <!-- Main JS File -->
      <script src="<?php echo get_template_directory_uri().'/assets/';?>js/main.js"></script>
      <script src="<?php echo get_template_directory_uri().'/assets/';?>js/demo-2.js"></script>
   </body>
</html>