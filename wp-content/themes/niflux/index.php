<?php
	get_header();
?>
<main class="main">
	<div class="intro-slider-container">
		<div class="owl-carousel owl-simple owl-light owl-nav-inside owl-loaded owl-drag" data-toggle="owl" data-owl-options="{&quot;nav&quot;: false}">
			<div class="owl-stage-outer">
				<div class="owl-stage" style="transform: translate3d(-2698px, 0px, 0px); transition: all 0s ease 0s; width: 9443px;">
					<?php
						$bannerArray = array( 'post_type' => 'home_banner','posts_per_page' => -1,'orderby' => 'date', 'order' => 'DESC' );
						$banner = new WP_Query( $bannerArray );
						if ( $banner->have_posts() ) : while ( $banner->have_posts() ) : $banner->the_post();
					?>
						<div class="owl-item" style="width: 1349px;">
							<div class="intro-slide" style="background-image: url(<?php echo get_the_post_thumbnail_url($banner->ID); ?>);">
								<div class="container intro-content">
									<?php echo get_the_content();  ?>
									<a href="<?php echo get_field( "button_link" ); ?>" class="btn btn-primary">
										<span><?php echo get_field( "button_lable" ); ?></span>
									</a>
								</div>
							</div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
		<span class="slider-loader text-white"></span>
	</div>
    <div class="mb-3 mb-lg-5"></div>
	<?php 
		global $post; 
		$frontCat = $frontCatName = '';
		$featuredCat = get_terms( 'product_cat', array( 'orderby'  => 'name' ) );
		foreach ($featuredCat as $value) {
			$show_home_page = get_term_meta($value->term_id, 'show_home_page', true);
			if($show_home_page){
				$frontCat = $value->term_id;
				$frontCatName = $value->name;
			}
		}
	?>   
	<div class="banner-group">
		<div class="container">
			<div class="row">
				<?php 
					$fetCount = 1;
					foreach ($featuredCat as $term) 
					{
						$fetured = get_term_meta($term->term_id, 'fetured_cat', true);
						if($fetured)
						{
							$thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
							$CatImage = wp_get_attachment_url( $thumbnail_id );
							$catUrl = site_url().'/product-category/'.$term->slug;
							?>
							<div class="col-sm-12 col-md-4 col-lg-4">
								<div class="banner banner-overlay">
									<a href="<?php echo $catUrl; ?>">
										<img src="<?php echo $CatImage;?>" alt="Banner">
									</a>
									<div class="banner-content banner-content-top bannerRightContent">
										<!-- End .banner-subtitle -->
										<h3 class="banner-title text-white"><?php echo $term->name; ?></h3>
										<?php if($term->description){ ?>
										<div class="banner-text "><?php echo $term->description; ?></div>
										<?php } ?>
										<a href="<?php echo $catUrl; ?>" class="btn banner-link">Shop Now<img src="<?php echo get_template_directory_uri().'/assets/';?>img/left-arrow.png"/></a>
									</div>
								</div>
							</div>
							<?php			
							$fetCount ++ ;
						}
					} 
				?>
			</div>
		</div>
	</div>
    <div class="mb-3"></div>
    <?php
		$args = array(
						'post_type'             => 'product',
						'post_status'           => 'publish',
						'ignore_sticky_posts'   => 1,
						'posts_per_page'        => '-1',
						'tax_query'             => array(
						array(
							'taxonomy'     		=> 'product_cat',
							'field' 			=> 'term_id', //This is optional, as it defaults to 'term_id'
							'terms'         	=> $frontCat,
							'operator'      	=> 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
						),
						array(
							'taxonomy'      	=> 'product_visibility',
							'field'         	=> 'slug',
							'terms'         	=> 'exclude-from-catalog', // Possibly 'exclude-from-search' too
							'operator'      	=> 'NOT IN'
						)
			)
		);
		$products = get_posts($args);
		$homeCatCount = 0;
	?>
	<div class="container">
		<h4 class="sectionTitle"><?php echo $frontCatName; ?></h4>
	</div>
	<div class="container">
		<div class="owl-carousel owl-theme customizeArrows">
			<?php 
				foreach($products as $product)
				{ 
					$regularPrice = get_post_meta( $product->ID, '_regular_price', true );
					$salePrice = get_post_meta( $product->ID, '_sale_price', true );	
					if($homeCatCount == 0 || $homeCatCount%2==0)
					{
						echo'<div class="item">';
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'full' );
					} ?>
					<div class="product-section">
						<div class="product-content">
							<div class="productImage">
								<a href="#"><img src="<?php echo $image[0];?>"/></a>
								<div class="hoverOverContent">
									<ul>
										<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li><?php //echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
										<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-data">
								<div class="productRating">
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="yellowstar"><i class="icon-star"></i></span>
									<span class="greystar"><i class="icon-star"></i></span>
								</div>
								<div class="productName">
									<h4><a href="<?php the_permalink($product->ID); ?>"><?php echo $product->post_title; ?></b></a></h4>
								</div>
								<div class="Price">
									<span class="productRate"><?php echo get_woocommerce_currency_symbol().' '.$regularPrice; ?></span>									
									<span class="btn-buy float-right"><a href="<?php the_permalink($product->ID); ?>">buy now</a></span>
								</div>
							</div>
						</div>
					</div>
					<?php 
					if($homeCatCount%2==1)
					{
						echo'</div>';
					}
					$homeCatCount++; 
				} 
			?>
		</div>
	</div></div>
    <div class="mb-5"></div>
    <div class="deal-container  mb-5">
		<div class="categoryShop pt-5 pb-15">
			<div class="container">
				<div class="heading heading-center mb-3">
					<h2 class="title">Shop By Category </h2>
				</div>
				<div class="categorySlider">
					<div class="owl-carousel owl-theme customizeArrows">
						<?php
							foreach ($featuredCat as $term) 
							{
								$show_home_page = get_term_meta($term->term_id, 'show_home_page', true);
								$fetured = get_term_meta($term->term_id, 'fetured_cat', true);
								if(!$show_home_page && !$fetured)
								{ 
									$frontCat = $term->term_id;
									$frontCatName = $term->name;
									$thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
									$CatImage = wp_get_attachment_url( $thumbnail_id );
									$catUrl = site_url().'/product-category/'.$term->slug; ?>			
									<div class="item">
										<div class="product-section">
											<div class="product-content">
												<div class="catImage">
													<a href="<?php echo $catUrl; ?>"><img src="<?php echo $CatImage;?>"/></a>
												</div>
												<div class="cat-data">
													<div class="productName">
														<h4><a href="<?php echo $catUrl; ?>"><?php echo $frontCatName; ?> <br><b>"<?php echo $term->description; ?>"</b></a></h4>
													</div>
												</div>
											</div>
										</div>
									</div> <?php				
								}
							}
						?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">  
			<div class="icon-boxes-container">
				<div class="container">
					<div class="row">
						<div class="col-sm-4 col-lg-4">
							<div class="icon-box icon-box-side">
								<span class="icon-box-icon text-white">
									<i class="icon-rocket"></i>
								</span>
								<div class="icon-box-content">
								<?php echo get_field( "shipment", 64 ); ?>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-lg-4">
							<div class="icon-box icon-box-side">
								<span class="icon-box-icon text-white">
									<i class="icon-thumbs-up"></i>
								</span>
								<div class="icon-box-content">
								<?php echo get_field( "saving_content", 64 ); ?>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-lg-4">
							<div class="icon-box icon-box-side">
								<span class="icon-box-icon text-white">
									<i class="icon-life-ring"></i>
								</span>
								<div class="icon-box-content">
								<?php echo get_field( "online_support", 64 ); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
	<div class="mb-6"></div>
		<div class="container">
			<div class="heading heading-center mb-3">
				<h2 class="sectionTitle">New Arrivals</h2>
			</div>
			<div class="arrivalsSlider">
				<div class="owl-carousel owl-theme customizeArrows">
					<?php
					$arg_new = array(
									'post_type'      => 'product',
									'post_status'    => 'publish',
									'numberposts'    => -1,
									'product_tag' 	 => 'new-arrivals' 
								);
					$productsx = get_posts($arg_new);
					foreach($productsx as $productx)
					{
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $productx->ID ), 'full' );
						$title = $productx->post_title;
						?>
						<div class="item">
							<div class="product-section">
								<div class="product-content">
									<div class="productImage">
										<a href="<?php the_permalink($productx->ID); ?>">
											<img src="<?php echo $image[0];?>"/>
										</a>
										<div class="hoverOverContent">
											<ul>
												<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
												<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="product-data">
										<div class="productRating">
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="yellowstar"><i class="icon-star"></i></span>
											<span class="greystar"><i class="icon-star"></i></span>
										</div>
										<div class="productName">
											<h4><a href="<?php the_permalink($productx->ID); ?>"><?php echo $title; ?></b></a></h4>
										</div>
										<div class="Price">
											<span class="productRate"><?php echo get_woocommerce_currency_symbol().' '.$regularPrice; ?></span>									
											<span class="btn-buy float-right"><a href="<?php the_permalink($productx->ID); ?>">buy now</a></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php 
					} ?>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="heading heading-center mb-3">
				<h2 class="sectionTitle">Hot Deals</h2>
			</div>
			<div class="arrivalsSlider">
				<div class="owl-carousel owl-theme customizeArrows">
				<?php
					$arg_hot = array(
									'post_type'      => 'product',
									'post_status'    => 'publish',
									'numberposts'    => -1,
									'product_tag' 	 => 'hot-deals' 
								);
					$productsx = get_posts($arg_hot);
					foreach($productsx as $productx)
					{
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $productx->ID ), 'full' );
						$title = $productx->post_title;
						?>




					<div class="item">
						<div class="product-section">
							<div class="product-content">
								<div class="productImage">
									<a href="<?php the_permalink($productx->ID); ?>"><img src="<?php echo $image[0];?>"/></a>
									<div class="hoverOverContent">
										<ul>
											<li class="squareIcon"><a href="#" class="wishlisht"><i class="icon-heart"></i></a></li>
											<li class="squareIcon"><a href="#" class="shoppingcart"><i class="icon-cart-plus"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="product-data">
									<div class="productRating">
										<span class="yellowstar"><i class="icon-star"></i></span>
										<span class="yellowstar"><i class="icon-star"></i></span>
										<span class="yellowstar"><i class="icon-star"></i></span>
										<span class="yellowstar"><i class="icon-star"></i></span>
										<span class="greystar"><i class="icon-star"></i></span>
									</div>
									<div class="productName">
										<h4><a href="<?php the_permalink($productx->ID); ?>"><?php echo $title; ?></b></a></h4>
									</div>
									<div class="Price">
										<span class="productRate"><?php echo get_woocommerce_currency_symbol().' '.$regularPrice; ?></span>									
										<span class="btn-buy float-right"><a href="<?php the_permalink($productx->ID); ?>">buy now</a></span>
									</div>
								</div>
							</div>
						</div>
					</div>


					<?php 
					} ?>


				</div>
			</div>
        </div>
    </main>
<?php
get_footer();
?>