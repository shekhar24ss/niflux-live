<!DOCTYPE html>
<html lang="en" style="">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Niflux</title>
      <meta name="keywords" content="HTML5 Template">
      <meta name="description" content="Molla - Bootstrap eCommerce Template">
      <meta name="author" content="p-themes">
      <!-- Favicon -->
      <link rel="apple-touch-icon" sizes="180x180" href="#">
      <link rel="icon" type="image/png" sizes="32x32" href="#">
      <link rel="icon" type="image/png" sizes="16x16" href="#">
      <meta name="theme-color" content="#ffffff">
      <link rel="stylesheet" href="css/line-awesome.min.css">
      <!-- Plugins CSS File -->
      <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/css/bootstrap.min.css'; ?>">
      <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/';?>css/owl.carousel.css">
      <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/';?>css/magnific-popup.css">
      <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/';?>css/jquery.countdown.css">
      <!-- Main CSS File -->
      <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/';?>css/style.css">
      <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/';?>css/skin-demo-2.css">
      <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/';?>css/demo-2.css">     
      <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/';?>fonts/stylesheet.css">     
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

   </head>
   <body data-gr-c-s-loaded="true" style="overflow-x: hidden;">
      <div class="page-wrapper">
         <header class="header header-2 header-intro-clearance">
            <div class="header-top">
               <div class="container">
                  <div class="header-left">
                  </div>
                  <!-- End .header-left -->
                  <div class="header-right">
                     <ul class="top-menu">
                        <li>
                           
                        <?php 
                              $args = array(
                                 'menu_class' => '',        
                                 'menu' => '(home-top-menu)'
                              );
                              wp_nav_menu( $args ); 
                           ?>
                        </li>
                     </ul>
                     <!-- End .top-menu -->
                  </div>
                  <!-- End .header-right -->
               </div>
               <!-- End .container -->
            </div>
            <!-- End .header-top -->
            <div class="header-middle">
               <div class="container">
                  <div class="header-left">
                     <button class="mobile-menu-toggler">
                     <span class="sr-only">Toggle mobile menu</span>
                     <i class="icon-bars"></i>
                     </button>
                     <a href="<?php echo site_url(); ?>" class="logo">
                     <img src="<?php echo get_template_directory_uri().'/assets/';?>img/logo.png" alt="Logo" >
                     </a>
                  </div>
                  <!-- End .header-left -->
                  <div class="header-center">
                     <div class="header-search header-search-extended header-search-visible header-search-no-radius d-none d-lg-block">
                        <a href="#" class="search-toggle" role="button"><i class="icon-search"></i></a>
                        <form action="#" method="get">
                           <div class="header-search-wrapper search-wrapper-wide">
                              <label for="q" class="sr-only">Search</label>
                              <input type="search" class="form-control" name="q" id="q" placeholder="Search product ..." required="">
                              <button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
                           </div>
                           <!-- End .header-search-wrapper -->
                        </form>
                     </div>
                     <!-- End .header-search -->
                  </div>
                  <div class="header-right">
                     <div class="account">
                        <a href="<?php echo site_url().'/my-account/'; ?>" title="My account">
                           <div class="icon">
                              <i class="icon-user"></i>
                           </div>
                           <p class="signinText"><span class="signInText">Sign In </span>My Account <span class="chevron"><img src="<?php echo get_template_directory_uri().'/assets/';?>img/up-chevron.png"/></span></p>
                        </a>
                     </div>
                      <div class="Compare-dropdown">
                        <a href="#" title="Compare">
                           <div class="icon">
                              <img src="<?php echo get_template_directory_uri().'/assets/';?>img/stack.png"/>
                           </div>
                           <p>Compare</p>
                        </a>
                     </div>
                     <!-- End .compare-dropdown -->
                     <div class="wishlist">
                        <a href="#" title="Wishlist">
                           <div class="icon">
                              <i class="icon-heart-o"></i>
                              <span class="wishlist-count badge">0</span>
                           </div>
                           <p>Wishlist</p>
                        </a>
                     </div>
                     <!-- End .compare-dropdown -->
                     <div class="dropdown cart-dropdown">
                        <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                           <div class="icon">
                              <i class="icon-shopping-cart"></i>
                              <span class="cart-count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                           </div>
                           <p>My Bag</p>
                        </a>
                        <!-- End .dropdown-menu -->
                     </div>
                     <!-- End .cart-dropdown -->
                  </div>
                  <!-- End .header-right -->
               </div>
               <!-- End .container -->
            </div>
            <!-- End .header-middle -->
            <div class="sticky-wrapper" style="">
               <div class="header-bottom sticky-header">
                  <div class="container">
                     <div class="header-left">
                        <div class="dropdown category-dropdown">
                           <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static" title="Browse Categories">
                           All Categories
                           </a>
                           <div class="dropdown-menu">
                              <nav class="side-nav">
                                 <!--ul class="menu-vertical sf-arrows sf-js-enabled" >
                                    <li class=""><a href="#"><i class="icon-long-arrow-right"></i>Filament Bulbs</a></li>
                                    <li class=""><a href="#"><i class="icon-long-arrow-right"></i>LED Surface Panel</a></li>
                                    <li><a href="#"><i class="icon-long-arrow-right"></i>LED Flood Light Focus</a></li>
                                    <li><a href="#"><i class="icon-long-arrow-right"></i>LED COB Downlight Classic</a></li>
                                    <li><a href="#"><i class="icon-long-arrow-right"></i>LED Street Lights Standard</a></li>
                                    <li><a href="#"><i class="icon-long-arrow-right"></i>LED T8 Tubes  Classic</a></li>
                                    <li><a href="#"><i class="icon-long-arrow-right"></i>LED Slim Panel Frisbees</a></li>
                                    <li><a href="#"><i class="icon-long-arrow-right"></i>LED Streer Lights Marvel </a></li>
                                 </ul-->
                                 <?php 
                                    $args = array(
                                       'menu_class' => 'menu-vertical sf-arrows sf-js-enabled',        
                                       'menu' => '(home-side-menu)',
                                       'link_before'   => '<i class="icon-long-arrow-right"></i>'
                                    );
                                    wp_nav_menu( $args ); 
                                 ?>
                                 <!-- End .menu-vertical -->
                              </nav>
                              <!-- End .side-nav -->
                           </div>
                           <!-- End .dropdown-menu -->
                        </div>
                        <!-- End .category-dropdown -->
                     </div>
                     <!-- End .header-left -->
                     <div class="header-right">
                        <nav class="main-nav">
                           <!--ul class="menu sf-arrows sf-js-enabled">
                              <li class="megamenu-container active">
                                 <a href="index.html" ><i class="icon-home"></i> Home</a>
                              </li>
                              <li class="">
                                 <a href="#" >Shop</a>
                              </li>
                              <li>
                                 <a href="#" >Products</a>
								            <ul>
                                        <li><a href="#">Filament Bulb</a></li>
                                        <li><a href="#">LED Bulb</a></li>
                                        <li><a href="#">LED Flood Light</a></li>
                                        <li><a href="#">LED COB Downlight</a></li>
                                        <li><a href="#">LED Street Lights</a></li>
                                    </ul>
                              </li>
                              <li>
                                 <a href="#" >Services</a> </li>
                              <li>
                                 <a href="#" >Contact Us</a>
                              </li>
                           </ul-->
                           <?php 
                              $args = array(
                                 'menu_class' => 'menu sf-arrows sf-js-enabled',        
                                 'menu' => '(main-menu)'
                              );
                              wp_nav_menu( $args ); 
                           ?>
                        </nav>
                        <?php //wp_nav_menu(array('menu' => 'main-menu', 'menu_class'=> 'menu', 'menu_class'=> 'sf-arrows', 'menu_class'=> 'sf-js-enabled', 'theme_location'=> 'header')); ?>
                     </div>
                     <!-- End .header-center -->
                     
                  </div>
                  <!-- End .container -->
               </div>
            </div>
            <!-- End .header-bottom -->
         </header>
         <!-- End .header -->
