<?php
if ( ! class_exists( 'Walker_Nav_Primary' ) ) {
  class Walker_Nav_Primary extends Walker_Nav_menu {

    public function start_lvl( &$output, $depth = 0, $args = array() ){ //ul
        $indent = str_repeat("\t",$depth);
        $submenu = ($depth > 0) ? ' sub-menu' : '';
        $output .= "\n$indent<ul class=\"dropdown-menu$submenu depth_$depth\">\n";
    }
}