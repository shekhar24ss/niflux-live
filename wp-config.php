<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */ 
define('WP_DEBUG', true);

define( 'FS_METHOD', 'direct' );
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'niflux' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0W^Ad8+& %hvxevz/lH]5i%ApoK0cj. e%Z>!i9]q/C1{|X!plL5eeCOq=n+QwI/' );
define( 'SECURE_AUTH_KEY',  '!G+:oy~%ZWcjw|nW&aC.7$dFy]`zzhNg7HTkeMFDi3)e=:(fX^1tYP;+eBvR8O*i' );
define( 'LOGGED_IN_KEY',    'lxV>Oq3u)DZt26H;_sAjsZCAeDm^HqO9tP5f`12k5`c=n[^MbQ#e=^?$Kq4NRLm_' );
define( 'NONCE_KEY',        'TjxV2p/Zp-otyZRQN<ARf5c9poahQ?.G&B#(]J nHK(3,Ntc(P|tuFm7)}}0$q8P' );
define( 'AUTH_SALT',        '<%LMRc4bKs>u^a:XwK1OPy$)-:DT*q=o-9HD[kR.Mi3O-|$LfxY(0(W*&V[.qwzq' );
define( 'SECURE_AUTH_SALT', 'gWBQltHKD]%DPr|W!4[rkh,%/T_?S3fBw+]pS:o#XDP$Z)1%%[DJ5eO{LcelB]YD' );
define( 'LOGGED_IN_SALT',   'dQe]=?].P)Cz(K.Gu7~3fXIAm`rCf{:7oj%&>KoIAZ^<BwxmP}d5l;7Mm3VLf>ea' );
define( 'NONCE_SALT',       '%?3aVLJ0hTE3DD$vDD<+hVwD5-z(.BzTy%6-/HxYT;%XHjDKA,sE<2:L[jB n9N,' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
